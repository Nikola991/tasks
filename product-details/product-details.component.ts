import { NGXLogger } from 'ngx-logger';

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { SystemMessageType } from '../../enums/systemMessageType.enum';
import { MetaTagsModel } from '../../models/metaTags.model';
import { OrderModel } from '../../models/order.model';
import { OrderItemModel } from '../../models/orderItem.model';
import { ProductModel } from '../../models/product.model';
import { AuthenticationService } from '../../services/authentication.service';
import { ConfigurationSettings } from '../../services/config.settings';
import { ProductService } from '../../services/product.service';
import { SeoService } from '../../services/seo.service';
import { ShoppingCartService } from '../../services/shoppingCart.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: [
    './scss/product-details.component.colors.scss',
    './scss/product-details.component.xs.scss',
    './scss/product-details.component.sm.scss',
    './scss/product-details.component.md.scss',
    './scss/product-details.component.lg.scss',
    './scss/product-details.component.xl.scss',
  ]
})
export class ProductDetailsComponent implements OnInit {
  product: ProductModel;
  productId: string;
  private sub: any;
  loaderVisible: boolean;
  quantity: number;
  order: OrderModel;
  requestFailed: boolean;
  systemMessageType;
  isAdmin: boolean;

  images;

  thisItemInCart: OrderItemModel;

  constructor(
    private router: Router,
    private productService: ProductService,
    private route: ActivatedRoute,
    private shoppingCartService: ShoppingCartService,
    private seoService: SeoService,
    private authServuce: AuthenticationService,
    private logger: NGXLogger
    ) {
      // changing reload strategy because of product promotion component 
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

  ngOnInit() {
    this.loaderVisible = true;

    this.isAdmin = this.authServuce.isAdmin();

    this.productService.getProductById(this.route.snapshot.params['id']).subscribe((result) => {
      this.product = result;

      this.seoService.setMetaTags(
        new MetaTagsModel(
          ConfigurationSettings.appConfig.shopName,
          this.product.productName,
          this.product.shortDescription,
          this.product.images.find(x => <boolean>x.isMain).src,
          this.product.urlName
      ));

    }, (error) => {
      this.logger.error(error);
        this.requestFailed = true;
    }, () => {
      this.order = this.shoppingCartService.getOrderFromLocalStorage();
      let quantity = 1;
      if (this.order) {
        this.thisItemInCart = this.order.orderItems.find(x => x.productId === this.product.id);
        if (this.thisItemInCart) {
          quantity = Number(this.thisItemInCart.quantity);
        } else {
          quantity = 1;
        }
      } else {
        quantity = 1;
      }
      this. quantity = quantity;

      if (this.loaderVisible) {
        this.loaderVisible = false;
      }

    });
  }

  promotionProductsLoadingFinished() {
    if  (this.product) {
      this.loaderVisible = false;
    }
  }

  activateDeactivate() {
    this.product.isDeactivated = !this.product.isDeactivated;
    this.systemMessageType = null;
    if (this.product.isDeactivated) {
      this.productService.deactivate(this.product.id).subscribe(
        (result) => {},
        (error) => { 
          this.logger.error(error);
          this.systemMessageType = SystemMessageType.Error; },
        () => { this.systemMessageType = SystemMessageType.Success; }
      );
    } else {
      this.productService.activate(this.product.id).subscribe(
        (result) => {},
        (error) => { 
          this.logger.error(error);
          this.systemMessageType = SystemMessageType.Error; },
        () => { this.systemMessageType = SystemMessageType.Success; }
      );
    }
  }

  addToCart() {
    let item = new OrderItemModel(
      this.product.id,
      this.product.productName,
      this.product.urlName,
      this.product.price,
      (Number(this.product.price) * this.quantity).toString(),
      this.quantity.toString(),
      this.product.images.find(x => x.isMain === true)
    );

    this.shoppingCartService.addOrderItem(item);
  }

  increaseQuantity() {
    this.quantity += 1;
  }

  decreaseQuantity() {
    if (this.quantity > 1) {
      this.quantity -= 1;
    }
  }
}
